#!/usr/bin/env python3
import PIL
from PIL import Image
import sys

basewidth = int(sys.argv[1])
i = 0
for arg in sys.argv:
	if(i > 1):
		fileName = arg
		img = Image.open(fileName);
		wpercent = (basewidth / float(img.size[0]))
		hsize = int((float(img.size[1]) * float(wpercent)))
		img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
		img.save(fileName)
	i+=1
print("Thanks for using this program")